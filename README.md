# Setup

## config.php
Incluir o código abaixo no arquivo `config.php` na raiz do projeto
```
// Página inicial do site (0: Site, 1: Painel, 2: Preferências do usuário)
$CFG->defaulthomepage = 0;

// Forçar login
$CFG->forcelogin = true;

// Define o tema padrão ** IMPORTANTE **
$CFG->theme = 'unifeso';

// Permitir que usuários usem o "dock"?
$CFG->allowblockstodock = false;`
```