# Doctrina - Moodle Theme

## Get started
Doctrina is automated with [GulpJS][gulp] and Sass.

### Install Gulp
If you haven't GulpJS in your system, install globally:
```sh
$ npm install -g gulp-cli
```

### Install Dependencies
To install all dependencies:
```sh
$ npm install
```

### Theme Dependency
We're using [Bootstrap][bootstrap-theme] theme by **bmbrand** as submodule.
Use `git submodule` to add the theme or clone into **your-project/theme/bootstrap**
```sh
$ git submodule add -b [MOODLE_lastversion_STABLE] https://github.com/bmbrands/theme_bootstrap.git theme/bootstrap
```
The branch `MOODLE_30_STABLE` is used for Moodle 3.0.

## Setup
### config.php
```php
// Force Login
$CFG->forcelogin = true;

// Theme Select
$CFG->theme = 'doctrina';

```

[gulp]: http://gulpjs.com
[bootstrap-theme]: https://github.com/bmbrands/theme_bootstrap
