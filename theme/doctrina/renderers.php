<?php
/**
 * Renderers to align Moodle's HTML with that expected by doctrina
 *
 * @package    theme_doctrina
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Course Renderer
require_once('renderers/course_renderer.php');

// Core Renderer
require_once('renderers/core_renderer.php');

