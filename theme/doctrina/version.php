<?php
defined('MOODLE_INTERNAL') || die;

// Recent plugin version
$plugin->version = 2017101400;

// Requirement moodle version
$plugin->requires  = 2015111000;

// Plugin Release
$plugin->release = '1.0.0';

// Plugin Name
$plugin->component = 'theme_doctrina';

$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2015111000,
);
