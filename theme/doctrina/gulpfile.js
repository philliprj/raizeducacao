// Packages
var gulp =		require("gulp"),
	sass = 		require("gulp-sass"),
	uglify = 	require("gulp-uglify"),
	concat = 	require("gulp-concat"),
	del = 		require("del");

var path = {
	styles: 	'src/scss/**/*.scss',
	scripts: 	['src/js/vendor/*.js','src/js/*.js'], // tip: add 'src/js/vendor/vendor.js' for vendors
}

// Uglify JS
gulp.task('jsmin', function(){
	gulp.src(path.scripts)
	.pipe(concat('main.js'))
	.pipe(uglify())
	.on('error', function(err) {
			console.log('ERROR:', err.message);
	})
	.pipe(gulp.dest('javascript'))
});

// Sass compiler and compress css
gulp.task('styles', function() {
	gulp.src(path.styles)
	.pipe(sass({
		outputStyle: 'compressed'
	})).on('error', function(err) {
			console.log('ERROR:', err.message);
	})
	.pipe(gulp.dest('style/'))
});

// Clean Task
gulp.task('clean', function(cb) {
    del(['javascript/'] ,cb)
});

// Default Task
gulp.task('default', ['clean'], function(){
	gulp.start('jsmin','styles')
});

// Gulp Watch
gulp.task('watch', function(){
	gulp.watch(path.styles, ['styles']);
	gulp.watch(path.scripts, ['jsmin']);
})
