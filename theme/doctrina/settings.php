<?php
/**
 * Settings for the doctrina theme
 */
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
	// Header logo
	$name = 'theme_doctrina/logo';
	$title = get_string('logo','theme_doctrina');
	$description = get_string('logodesc', 'theme_doctrina');
	$setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
	$setting->set_updatedcallback('theme_reset_all_caches');
	$settings->add($setting);

	// Copyright Logo and text
	$name = 'theme_doctrina/ft_copyright';
	$title = get_string('ft_copyright', 'theme_doctrina');
	$description = get_string('ft_copyrightdesc', 'theme_doctrina');
	$setting = new admin_setting_confightmleditor($name, $title, $description, '');
	$setting->set_updatedcallback('theme_reset_all_caches');
	$settings->add($setting);

	// Contact and Info
	$name = 'theme_doctrina/ft_contact';
	$title = get_string('ft_contact', 'theme_doctrina');
	$description = get_string('ft_contactdesc', 'theme_doctrina');
	$setting = new admin_setting_confightmleditor($name, $title, $description, '');
	$setting->set_updatedcallback('theme_reset_all_caches');
	$settings->add($setting);

	// Content bottom
	$name = 'theme_doctrina/contentbottom';
	$title = get_string('contentbottom', 'theme_doctrina');
	$description = get_string('contentbottom_desc', 'theme_doctrina');
	$setting = new admin_setting_confightmleditor($name, $title, $description, '');
	$setting->set_updatedcallback('theme_reset_all_caches');
	$settings->add($setting);

	// Social media
	$name = 'theme_doctrina/ft_social';
	$title = get_string('ft_social', 'theme_doctrina');
	$description = get_string('ft_socialdesc', 'theme_doctrina');
	$setting = new admin_setting_configtextarea($name, $title, $description, '');
	$setting->set_updatedcallback('theme_reset_all_caches');
	$settings->add($setting);

	// Primary color
	$name = 'theme_doctrina/primarycolor';
	$visiblename = get_string('primarycolor', 'theme_doctrina');
	$description = get_string('primarycolor_desc', 'theme_doctrina');
	$setting = new admin_setting_configcolourpicker($name, $visiblename, $description, null);
	$setting->set_updatedcallback('theme_reset_all_caches');
	$settings->add($setting);

	// Primary color
	$name = 'theme_doctrina/buttoncolor';
	$visiblename = get_string('buttoncolor', 'theme_doctrina');
	$description = get_string('buttoncolor_desc', 'theme_doctrina');
	$setting = new admin_setting_configcolourpicker($name, $visiblename, $description, null);
	$setting->set_updatedcallback('theme_reset_all_caches');
	$settings->add($setting);

	// Custom CSS Content
	$name = 'theme_doctrina/customcss';
	$title = get_string('customcss', 'theme_doctrina');
	$description = get_string('customcss_desc', 'theme_doctrina');
	$setting = new admin_setting_configtextarea($name, $title, $description, '');
	$setting->set_updatedcallback('theme_reset_all_caches');
	$settings->add($setting);
}
