<?php

defined('MOODLE_INTERNAL') || die();

abstract class Course
{
    /**
     * @param $category
     * @param string $path
     * @return array
     */
    public static function get($category, $path='')
    {
        // all courses
        if (empty($category)) {

            return Course::all();
        }

        // reorder courses
        return Course::sort(
            // merge courses 'father' and 'childs'
            array_merge(
                Course::parent($category),
                Course::children($path)
            )
        );
    }

    /**
     * Return all courses
     * @return array
     */
    private static function all()
    {
        global $DB;

        $query_all_courses = <<<SQL
SELECT
 course.id,
 categories.name,
 course.fullname,
 course.sortorder
FROM {course_categories} categories
  INNER JOIN {course} course
    ON categories.id = course.category
WHERE
  categories.visible = :visiblecategory AND
  course.visible = :visiblecourse
ORDER BY
  course.id DESC,
  course.sortorder ASC

SQL;
        return (array)$DB->get_records_sql($query_all_courses, array('visiblecategory' => true, 'visiblecourse' => true));
    }

    /**
     * Return course fomr parent category
     * @param $category
     * @return array
     */
    private static function parent($category)
    {
        global $DB;

        $query_parent_courses = <<<SQL
SELECT
 course.id,
 categories.name,
 course.fullname,
 course.sortorder
FROM {course_categories} categories
  INNER JOIN {course} course
    ON categories.id = course.category
WHERE
  course.category = :categoryid AND
  categories.visible = :visiblecategory AND
  course.visible = :visiblecourse
SQL;
        return (array)$DB->get_records_sql($query_parent_courses, array('categoryid' => $category,'visiblecategory' => true, 'visiblecourse' => true));
    }


    /**
     * Return child courses from tree or path
     * @param $path
     * @return array
     */
    private static function children($path)
    {
        global $DB;

        $query_children_courses = <<<SQL
SELECT
  course.id,
  categories.name,
  course.fullname,
  course.sortorder
FROM {course_categories} categories
  INNER JOIN {course} course
    ON categories.id = course.category
WHERE
  categories.path LIKE '$path/%' AND
  categories.visible = :visible_categorie AND
  course.visible = :visible_course

SQL;

        return (array)$DB->get_records_sql($query_children_courses, array(
            'visible_categorie' => true,
            'visible_course' => true)
        );
    }

    private static function sort($data, $column = 'id')
    {
        usort($data, function($a, $b) use($column) {
            return $a->$column < $b->$column ? 1: -1;
        });

        return $data;
    }
}
