<?php
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/coursecatlib.php');
require_once($CFG->libdir . '/datalib.php');

abstract class Category
{

	/**
	 * Busca uma determinada categoria
	 *
	 * @param $id
	 * @return coursecat|null
	 * @throws moodle_exception
	 */
	public static function get($id = false)
	{
		if ($id == false)
			return array();
		return coursecat::get($id);
	}

	/**
	 * Retorna as categorias
	 *
	 * @param $category
	 * @return array
	 */
	public static function get_categories($categoryid = 0)
	{
		global $DB;

		return $DB->get_records_sql("SELECT * FROM {course_categories} WHERE parent = $categoryid AND visible = :visible ORDER BY name,sortorder", array('visible' => true));
	}

	/**
	 * Busca todos os cursos da página de categoria
	 *
	 * @param bool $category
	 * @return array
	 * @throws moodle_exception
	 */
	public static function get_courses($category = false, $visible = true)
	{
		global $DB;

		if (empty($category)) {
			$sql = 'SELECT * FROM {course_categories} categories
					  INNER JOIN {course} course
						ON categories.id = course.category
					  WHERE
						categories.visible = :visible
					ORDER BY course.id DESC,course.sortorder ASC';
		} else {
			$navigation = Category::get($category)->path;
			$sql = "SELECT
						CONCAT(course.category, '-', course.id) as id,
						course.*,
						categories.name
					FROM {course_categories} categories
					  INNER JOIN {course} course
						ON categories.id = course.category
					WHERE
					  categories.path LIKE '$navigation/%'
					  OR categories.id = $category AND
					  categories.visible = :visible
					ORDER BY course.id DESC, course.sortorder ASC";
		}

		$result = $DB->get_records_sql($sql, array('visible' => true));

		return array_map(
			function ($row) {
				$row->thumb = Category::get_courseimage($row->id);
				return $row;
			}, $result);
	}

	/**
	 * Retorna o breadcrumb já formatado baseado em todas as categorias já consultadas
	 *
	 * @param array $categories
	 */
	public static function set_breadcrumb()
	{
		if (self::categories_count() > 0) {

			self::set_breadcrumb_default(true, '/course/index.php');
			self::set_breadcrumb_for_categories();
		} else {

			self::set_breadcrumb_default();
		}

		return;
	}

	/**
	 * Seta o breadcrumb padrão "Página inicial > Categorias de Cursos"
	 *
	 * @param bool $ignoreactive
	 * @param null $link
	 * @throws coding_exception
	 */
	public static function set_breadcrumb_default($ignoreactive = true, $link = null)
	{
		global $PAGE;

		if ($ignoreactive) {
			$PAGE->navbar->ignore_active();
		}

		if (is_null($link)) {
			$PAGE->navbar->add(get_string('course_category', 'theme_doctrina'));
			return;
		}

		$PAGE->navbar->add(get_string('course_category', 'theme_doctrina'), new moodle_url($link));
		return;
	}

	/**
	 * Verifica se existe algum elemento dentro do array $PAGE->categories
	 *
	 * @return bool
	 */
	public static function categories_count()
	{
		global $PAGE;

		return (int)count($PAGE->categories);
	}

	/**
	 * Adiciona ao breadcrumb as categorias baseado no id da categoria consultado!
	 */
	public static function set_breadcrumb_for_categories()
	{
		global $PAGE;

		// Reverte o array, pois o moodle envia os objetos na ordem oposta
		$categories = array_reverse($PAGE->categories, true);

		// Inicia o incremento
		$i = 1;

		// Contagem de categorias
		$count = self::categories_count();

		foreach ($categories as $item) {

			// Recebe o array com os valores de cada categoria
			$navbar = $count == $i ? array('itemname' => $item->name, 'link' => null) : array('itemname' => $item->name, 'link' => new moodle_url('/course/index.php?categoryid=' . $item->id));

			// Adiciona ao breadcrumb um novo nó
			$PAGE->navbar->add($navbar['itemname'], $navbar['link']);

			// Valida somente no último "row", para não adicionar link ao mesmo
			$i++;
		}

		return;
	}

	public static function get_courseimage($courseId)
	{
	    $fs = get_file_storage();
	    $files = $fs->get_area_files(
	        $contextid = context_course::instance($courseId)->id,
	        $component = 'course',
	        $filearea = 'overviewfiles',
	        $itemid = false,
	        $sort = 'filename',
	        $includedirs = true
	    );

	    $files = array_filter($files, function ($file) {
	        return $file->is_valid_image();
	    });

	    if (empty($files)) {
	        return false;
	    }

	    $file = reset($files);
	    $image = (string)moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), '', '', $file->get_filename());
	   	return $image;
	}
}
