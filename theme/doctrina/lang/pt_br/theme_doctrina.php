<?php
/**
 * @package    theme_doctrina
 * @copyright  Bruno Rodrigues
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Doctrina';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['region-after-header'] = 'Header';
$string['fullscreen'] = 'Full screen';
$string['closefullscreen'] = 'Close full screen';
$string['choosereadme'] = '
	<p>doctrina theme.</p>
';
$string['welcome'] = 'Bem-vindo(a)';

// Logo
$string['logo'] = 'Logo';
$string['logodesc'] = 'Insira sua logo, altura max. 100px';

// Search forms
$string['search'] = 'Pesquisar';
$string['search_courses'] = 'Pesquisar cursos...';

// Footer - Copyright
$string['ft_copyright'] = 'Copyright (logo e copyright)';
$string['ft_copyrightdesc'] = 'Breve descrição da empresa e direitos reservados.';
$string['all_rights_reserved'] = 'Todos os direitos reservados.';

// Footer - Contact
$string['ft_contact'] = 'Contato';
$string['ft_contactdesc'] = 'Informações de contato como telefones e emails';

// Footer - Social Media Links
$string['ft_social'] = 'Redes Sociais';
$string['ft_socialdesc'] = 'Separar os links com vírgula( , )
Ex.:<br><code>http://twitter.com/seutwitter,<br>
http://facebook.com/seufacebook,<br>
http://instagram.com/seuinstagram,<br>
http://linkedin.com/seulinkedin<br></code>';

// Footer - Html Custom
$string['ft_content'] = 'Conteúdo Customizado - HTML';
$string['ft_contentdesc'] = '';

// Login Page
$string['login_signin_description'] = 'Preencha os campos abaixo com seus dados corretamente.';
$string['forgot_password'] = 'Esqueceu seu nome de usuário ou senha?';
$string['signup'] = 'Cadastrar';
$string['password'] = 'Senha';
$string['user'] = 'Usuário';
$string['access_as_guest'] = 'Accessar como visitante';

// Courses and Category
$string['all_courses'] = "Todos os Cursos";
$string['empty_courses'] = "Não há cursos nesta categoria";
$string['course_category'] = "Catálogo de Cursos";
$string['all_categories'] = "Todas as Categorias";
$string['select_category'] = "Selecione uma categoria";
$string['categories'] = "Categorias";

// System
$string['homepage'] = "Página Inicial";
$string['messages'] = "Mensagens";
$string['courses'] = "Cursos";
$string['notifications'] = "Notificações";
$string['calendar'] = "Calendário";
$string['select'] = "Selecione";

// Settings
$string['primarycolor'] = "Cor Primária";
$string['primarycolor_desc'] = "Cor Primária do tema, incluindo sidebar, barra de filtros, etc...";
$string['buttoncolor'] = "Botões principais";
$string['buttoncolor_desc'] = "Background de botões primários";
$string['customcss'] = "CSS customizado";
$string['customcss_desc'] = "Se você não tem conhecimentos em CSS, consulte um profissional.";
$string['contentbottom'] = "Content bottom";
$string['contentbottom_desc'] = "Content below the frontpage course cards";
