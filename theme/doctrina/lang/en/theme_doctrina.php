<?php
/**
 * @package    theme_doctrina
 * @copyright  Bruno Rodrigues
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Doctrina';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['region-after-header'] = 'Header';
$string['fullscreen'] = 'Full screen';
$string['closefullscreen'] = 'Close full screen';
$string['choosereadme'] = '
	<p>doctrina theme.</p>
';
$string['welcome'] = 'Welcome';

// Logo
$string['logo'] = 'Logo';
$string['logodesc'] = 'Insert logo, max. height: 100px';

// Search forms
$string['search'] = 'Search';
$string['search_courses'] = 'Search courses...';

// Footer - Copyright
$string['ft_copyright'] = 'Copyright';
$string['ft_copyrightdesc'] = 'Description to footer copyright.';
$string['all_rights_reserved'] = 'All rights reserved.';

// Footer - Contact
$string['ft_contact'] = 'Contact';
$string['ft_contactdesc'] = 'Contact info, tels and emails.';

// Footer - Social Media Links
$string['ft_social'] = 'Social Media Links';
$string['ft_socialdesc'] = 'Separate links with commas( , )
Ex.:<br><code>http://twitter.com/yourtwitter,<br>
http://facebook.com/yourfacebook,<br>
http://instagram.com/yourinstagram,<br>
http://linkedin.com/yourlinkedin<br></code>';

// Footer - Html Custom
$string['ft_content'] = 'Custom Content - HTML';
$string['ft_contentdesc'] = 'Sed posuere consectetur est at lobortis.';

// Login Page
$string['login_signin_description'] = 'Fill fields below with your correctly info.';
$string['forgot_password'] = 'Forgot your username or password?';
$string['signup'] = 'Signup';
$string['password'] = 'Password';
$string['user'] = 'User';
$string['access_as_guest'] = 'Access as a Guest';

// Courses and Category
$string['all_courses'] = "All Courses";
$string['empty_courses'] = "There are no courses registered in this category.";
$string['course_category'] = "Courses";
$string['categories'] = "Categories";
$string['all_categories'] = "All Categories";
$string['select_category'] = "Select a category";

// System
$string['homepage'] = "Home";
$string['messages'] = "Messages";
$string['courses'] = "Courses";
$string['notifications'] = "Notifications";
$string['calendar'] = "Calendar";
$string['select'] = "Select";

// Settings
$string['primarycolor'] = "Primary Color";
$string['primarycolor_desc'] = "Theme primary color, color from sidebar, filter, etc...";
$string['buttoncolor'] = "Primary buttons background";
$string['buttoncolor_desc'] = "primary buttons background";
$string['customcss'] = "CSS custom";
$string['customcss_desc'] = "If you dont know CSS, tell with a developer.";
$string['contentbottom'] = "Content bottom";
$string['contentbottom_desc'] = "Conteúdo abaixo dos cards de cursos da frontpage";
