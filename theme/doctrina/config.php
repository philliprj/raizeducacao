<?php
/**
 * Doctrina
 * @package   theme_doctrina
 * @copyright Bruno Rodrigues
 */

$THEME->name = 'doctrina';
$THEME->doctype = 'html5';
// Sheets
$THEME->parents = array('bootstrap');
$THEME->sheets = array('moodle', 'style');
$THEME->supportscssoptimisation = false;
$THEME->enable_dock = false;
$THEME->rendererfactory = 'theme_overridden_renderer_factory';

// Custom Pages
$THEME->layouts = array(
	'frontpage' => array(
        'file' => 'frontpage.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
        'options' => array('nonavbar' => true),
    ),
    'course' => array(
        'file' => 'course.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
        'options' => array('langmenu' => true),
    )
);

// JS
$THEME->javascripts = array(
    'main'
);
