<!-- Slideshow -->
<?php global $SLIDESHOW; ?>
<?php if($SLIDESHOW->getSlides()): ?>
    <section id="slideshow" class="hidden-sm hidden-xs clearfix">
        <ul class="slides">
            <?php foreach ($SLIDESHOW->getSlides() as $slide): ?>
                <li class="slide-item">
                    <a class="slide-link" style="background-image: url(<?php echo $slide->image_url; ?>)" href="<?php echo $slide->url; ?>" title="<?php echo $slide->title; ?>" target="_blank">
                        <?php if(!empty($slide->title) && !empty($slide->text)): ?>
	                        <div class="slide-caption">
	                            <h3 class="slide-title"><?php echo $slide->title;?></h3>
								<span class="slide-description dotdot"><?php echo $slide->text;?></span>
	                        </div>
						<?php endif;  ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
	</section>
<?php endif; ?>
<!-- Slideshow -->
