<div id="main-navigation">
	<ul>
		<li>
			<a href="" class="toggle-sidebar-menu">
				<div class="hamburg">
					<span></span>
				</div>
				Menu
			</a>
		</li>
		<li>
			<a href="/course">
				<img class="nav-icon" src="<?php echo $OUTPUT->pix_url('icons/courses-white', 'theme'); ?>" alt="Courses">
				<?php echo get_string('courses', 'theme_doctrina'); ?>
			</a>
		</li>
		<li>
			<a href="/message">
				<img class="nav-icon" src="<?php echo $OUTPUT->pix_url('icons/messages-white', 'theme'); ?>" alt="Messages">
				<?php echo get_string('messages', 'theme_doctrina'); ?>
			</a>
		</li>
		<li>
			<a href="/calendar/view.php?view=month">
				<img class="nav-icon" src="<?php echo $OUTPUT->pix_url('icons/calendar-white', 'theme'); ?>" alt="Calendar">
				<?php echo get_string('calendar', 'theme_doctrina'); ?>
			</a>
		</li>
	</ul>
</div>

	<div id="sidebar-content">
		<!-- Profile Card -->
	<div id="profile-card" class="clearfix hidden-md hidden-lg">
		<!-- Search -->
		<div id="search-sidebar" class="clearfix">
			<form method="GET" action="<?php echo new moodle_url('/course/search.php'); ?>" role="search">
				<input name="search" type="text" class="search-form-field" placeholder="<?php echo get_string('search_courses', 'theme_doctrina'); ?>">
				<button type="submit" class="glyphicon glyphicon-search"></button>
			</form>
		</div>

		<!-- Picture -->
		<?php if(isloggedin()): ?>
			<div class="profile-pic">
				<?php echo $OUTPUT->user_picture($USER, array('size' => 45)); ?>
			</div>
		<?php endif; ?>

		<!-- Username -->
		<div class='profile-username'>
			<span><?php echo get_string('welcome', 'theme_doctrina') ?></span><br>
			<?php echo fullname($USER);?>
		</div>

		<!-- Edit Profile Button -->
		<a class="profile-edit glyphicon glyphicon-pencil" href="<?php echo new moodle_url('/user/edit.php')?>"></a>
	</div>

	<!-- Custom Menu -->
	<div id="main-menu">
		<h2>Menu</h2>
		<ul>
			<li>
				<a href="/"><i class="glyphicon glyphicon-home"></i><?php echo get_string('homepage',  'theme_doctrina') ?></a>
			</li>
			<li>
				<a href="<?php echo new moodle_url('/course/index.php')?>"><i class="glyphicon glyphicon-th-list"></i><?php echo get_string('all_courses',  'theme_doctrina') ?></a>
			</li>
			<li>
				<a href="<?php echo new moodle_url('/message/index.php')?>"><i class="glyphicon glyphicon-envelope"></i><?php echo get_string('messages',  'theme_doctrina') ?></a>
			</li>

			<?php echo $OUTPUT->custom_menu(); ?>

			<div class="hidden-md hidden-lg">
				<?php if(isloggedin()) { ?>
					<li class="logoutlink">
						<a class="logout" href="<?php echo new moodle_url('/login/logout.php')?>"><i class="glyphicon glyphicon-off"></i> Logout</a>
					</li>
				<?php } else { echo $OUTPUT->user_menu(); }?>
			</div>
		</ul>
	</div>

	<!-- Contents -->
	<?php
		// Get Sidebar
		if ($knownregionpre) {
			echo $OUTPUT->blocks('side-pre');
		}
	?>
	</div>
