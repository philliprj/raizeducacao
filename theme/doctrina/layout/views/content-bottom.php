<?php if(@$PAGE->theme->settings->contentbottom) : ?>
<section id="content-bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="description">
					<?php echo $PAGE->theme->settings->contentbottom; ?>
				</p>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
