<!-- Header -->
<?php
$search_classes = "col-md-push-1 col-md-5";
$loginbtn_classes = "col-md-push-1 col-md-3";
if (!isloggedin()) {
	$search_classes = "col-md-push-2 col-md-5";
	$loginbtn_classes = "col-md-2 col-md-push-2";
}
?>
<header id="header">
	<div class="container-fluid">
		<div class="row">
			<!-- Logo -->
			<div id="header-logo" class="col-md-3">
				<h1>
					<a id="logo" href="<?php echo new moodle_url('/?redirect=0')?>" title="Home">
						<?php
							// Show logo
							if($header_logo) {
								echo '<img src="'.$header_logo.'" alt="Logo'.$SITE->fullname.'">';
							} else {
								echo '<span>' .$SITE->fullname.'</span>';
							}
						?>
					</a>
				</h1>
			</div>


				<!-- Serach input -->
				<div id="header-search" class="<?php echo $search_classes;?>">
					<form id="search-form" class="clearfix" method="GET" action="<?php echo new moodle_url('/course/search.php'); ?>" role="search">
						<input name="search" type="text" class="search-form-field" placeholder="<?php echo get_string('search_courses', 'theme_doctrina'); ?>">
						<i class="glyphicon glyphicon-search"></i>
					</form>
				</div>

				<!-- User Info -->
				<div id="header-userinfo" class="<?php echo $loginbtn_classes;?>">
					<!-- Picture -->
					<?php echo $OUTPUT->user_menu(); ?>
				</div>

		</div>
	</div>
</header>
