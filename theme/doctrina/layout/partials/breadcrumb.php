<!-- breadcrumb -->
<div id="page-navbar" class="clearfix">
	<nav class="breadcrumb-nav" role="navigation" aria-label="breadcrumb"><?php echo $OUTPUT->navbar(); ?></nav>
	<div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
</div>
<!-- /breadcrumb -->
