<!-- Head -->
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <?php echo $OUTPUT->standard_head_html(); ?>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimal-ui">

	<?php
		$primarycolor =  @$PAGE->theme->settings->primarycolor;
		$buttoncolor =  @$PAGE->theme->settings->buttoncolor;
		$customcss =  @$PAGE->theme->settings->customcss;
	?>
	<?php if(isset($primarycolor)): ?>
		<style>
			#main-navigation,
			#main-navigation ul a:hover, #main-navigation ul a:focus,
			#main-menu h2,
			#profile-card,
			.category-filter,
			.profile_tree section h3, .profile_tree section .coursebox>.info .coursename a, .coursebox>.info .coursename .profile_tree section a, .profile_tree section .coursebox .panel-heading .coursename a, .coursebox .panel-heading .coursename .profile_tree section a {
				background: <?php echo $primarycolor; ?>
			}

			#header-userinfo .usermenu
			.usertext {
				color: <?php echo $primarycolor; ?>
			}

			#header-userinfo
			.avatar {
				border-color: <?php echo $primarycolor;?>
			}

			#search-sidebar input, #search-sidebar button {
				color: #fff;
			}

			::-webkit-input-placeholder { /* Chrome/Opera/Safari */
			  color: #909090 !important;
			}
			::-moz-placeholder { /* Firefox 19+ */
			  color: #909090 !important;
			}
			:-ms-input-placeholder { /* IE 10+ */
			  color: #909090 !important;
			}
			:-moz-placeholder { /* Firefox 18- */
			  color: #909090 !important;
			}
			.c-nav-tabs {
				border-color: <?php echo $primarycolor; ?>
			}
			.c-nav-tabs .nav-link.active {
				background-color: <?php echo $primarycolor; ?>
			}
		</style>
	<?php endif ?>

	<?php if(isset($buttoncolor)) { ?>
		<style>
			input[type="submit"].btn.btn-primary, input[type="submit"], #notice .singlebutton+.singlebutton input[type="submit"], .submit.buttons input[type="submit"][name="cancel"], input[type="submit"]#loginbtn, .guestsub input[type="submit"], .signuppanel .signupform input[type="submit"], .path-message .box.message input[type="submit"], button.btn.btn-primary, button.btn-primary.btn-courses, button#loginbtn, .btn.btn-primary, .btn-primary.btn-courses, input[type="submit"], #notice .singlebutton+.singlebutton input.btn-primary, #notice .singlebutton+.singlebutton input[type="submit"], #notice .singlebutton+.singlebutton input#loginbtn, .submit.buttons input.btn-primary[name="cancel"], .submit.buttons input[name="cancel"][type="submit"], .usermenu .login a, #header-userinfo .guest li a, #loginbtn, input[type="submit"]#loginbtn, .guestsub input[type="submit"], .signuppanel .signupform input[type="submit"], .path-message .box.message input[type="submit"],input[type="submit"].btn.btn-primary:hover, input[type="submit"]:hover, #notice .singlebutton+.singlebutton input[type="submit"]:hover, .submit.buttons input[type="submit"][name="cancel"]:hover, input[type="submit"]#loginbtn:hover, .signuppanel .signupform input[type="submit"]:hover, .path-message .box.message input[type="submit"]:hover, button.btn.btn-primary:hover, button.btn-primary.btn-courses:hover, button#loginbtn:hover, .btn.btn-primary:hover, .btn-primary.btn-courses:hover, input[type="submit"]:hover, #notice .singlebutton+.singlebutton input.btn-primary:hover, #notice .singlebutton+.singlebutton input[type="submit"]:hover, #notice .singlebutton+.singlebutton input#loginbtn:hover, .submit.buttons input.btn-primary[name="cancel"]:hover, .submit.buttons input[name="cancel"][type="submit"]:hover, .usermenu .login a:hover, #header-userinfo .guest li a:hover, #loginbtn:hover, input[type="submit"]#loginbtn:hover, .signuppanel .signupform input[type="submit"]:hover, .path-message .box.message input[type="submit"]:hover {
					background: <?php echo $buttoncolor; ?> !important;
				}
		</style>
	<?php } ?>

	<?php if(!empty($customcss)) { ?>
		<style>
			<?php echo $customcss; ?>
		</style>
	<?php } ?>

</head>
