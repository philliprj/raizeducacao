<!-- Footer -->
<footer id="footer">
	<div class="container-fluid">
		<div class="row">
			<!-- Copyright -->
			<?php if($ft_copyright !== '') { ?>
				<div class="col-md-<?php echo $countColumn?>" col-sm-12>
					<?php echo $ft_copyright; ?>
				</div>
			<?php } else { ?>
				<div class="col-md-4 col-sm-12 copyright-static">
					<?php
						$countColumn = 4;
						// Show logo
						if($header_logo) {
							echo '<img src="'.$header_logo.'" height="80"  alt="Logo'.$SITE->fullname.'">';
						} else {
							echo '<span>' .$SITE->fullname.'</span>';
						}
						echo '<p>Copyright &copy; ' .$SITE->fullname. ' | ' .get_string('all_rights_reserved', 'theme_doctrina'). '</p>';
					?>

				</div>
			<?php }?>

			<!-- Contact Info -->
			<?php if($ft_contact !== '') :?>
				<div class="col-md-<?php echo $countColumn?> hidden-sm hidden-xs">
					<h3><?php echo get_string('ft_contact', 'theme_doctrina'); ?></h3>
					<?php echo $ft_contact; ?>
				</div>
			<?php endif; ?>

			<!-- Social Media -->
			<?php if($ft_social !== '') :?>
				<div class="col-md-<?php echo $countColumn?> col-sm-12">
					<h3 class="hidden-sm hidden-xs"><?php echo get_string('ft_social', 'theme_doctrina'); ?></h3>
					<ul class="social-icons">
						<?php foreach ($mediaLinks as $mediaUrl) {
							$urlPrefixes = array('http://', 'https://', 'www.');
							$newUrl = str_replace($urlPrefixes, '', $mediaUrl);
							$mediaName = trim(explode('.',$newUrl)[0]);
							echo
								'<li class="'.$mediaName.'">
									<a target="_blank" class="fa fa-'.$mediaName.'" href="'.$mediaUrl.'">'.$mediaName.'</a>
								</li>';
							}
						?>
					</ul>
				</div>
			<?php endif;?>
		</div>
	</div>
</footer>

<?php echo $OUTPUT->standard_end_of_body_html() ?>
