<!-- Settings -->
<?php
$coursethumb;
if (!empty($course->thumb)): $coursethumb = $course->thumb; endif; ?>

<section id="category-page">

	<!-- Page Title -->
	<h2><?php echo get_string('course_category', 'theme_doctrina'); ?></h2>

	<div class="category-filter">
		<!-- Select button with Categories -->
		<label><?php echo get_string('select_category',  'theme_doctrina') ?>:</label>
		<div class="btn-group" role="group">
			<button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<?php echo get_string('select',  'theme_doctrina') ?> <i class="glyphicon glyphicon-chevron-down"></i>
			</button>
			<ul class="dropdown-menu">
				<li>
					<a href="<?php echo new moodle_url('/course/index.php', array('categoryid' => 0));?>"><?php echo get_string('all') ?></a>
				</li>
				<?php
				foreach ($categories as $category):?>
					<li>
						<a href="<?php echo new moodle_url('/course/index.php', array('categoryid' => $category->id)); ?>">
							<?php echo $category->name; ?>
						</a>
					</li>

				<?php endforeach; ?>
			</ul>
		</div>
	</div>

	<!-- Course Boxes -->
	<div class="row">
		<?php
		if ($courses):
			foreach ($courses as $course):
		?>

		<div class="col-lg-3 col-md-4 col-sm-6">
			<div class="coursebox">
				<div class="info">
					<div class="coursebox-category dotdot"><?php echo $course->name; ?></div>
					<h3 class="coursename">
						<a href="<?php echo new moodle_url('/course/view.php', array('id' => $course->id)); ?>">
							<?php echo $course->fullname; ?>
						</a>
					</h3>

					<div class="content">
						<div class="summary">
							<?php echo $course->summary;?>
						</div>
						<!-- Thumb -->
						<div class="courseimage" style="background-image: url(<?php echo $course->thumb; ?>)"></div>
                    </div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
		<?php else: ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					<span class="sr-only">Error:</span>

					<?php echo get_string('empty_courses', 'theme_doctrina'); ?>

				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
