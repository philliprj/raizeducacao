<?php
include($CFG->dirroot . '/theme/' . $PAGE->theme->name . '/theme_config.php');

$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);

$knownregionpre = $PAGE->blocks->is_known_region('side-pre');
$knownregionpost = $PAGE->blocks->is_known_region('side-post');

$regions = bootstrap_grid($hassidepre, false);

$PAGE->set_popup_notification_allowed(false);

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>

<!-- html html_header -->
<?php include "partials/head.php"; ?>
<body <?php echo $OUTPUT->body_attributes(); ?>>
	<?php echo $OUTPUT->standard_top_of_body_html() ?>

	<!-- Id Page -->
	<?php include "partials/header.php"; ?>

	<div id="page" class="container-fluid">
		<!-- Slideshow -->
		<?php // include "views/frontpage-slideshow.php"; ?>

	    <header id="page-header" class="clearfix">
	        <!-- breadcrumb -->
	        <?php include "partials/breadcrumb.php"; ?>

	        <div id="course-header">
	        	<?php echo $OUTPUT->course_header(); ?>
	        </div>
	    </header>

	    <div id="page-content">
	    	<div class="row">
				<!-- Main Content -->
				<div id="region-main" class="col-md-12">
				    <a href="<?php echo new moodle_url('/course');?>" class="btn-courses">
				    	<?php echo get_string('all_courses','theme_doctrina');?>
				    </a>
				    <?php
				    echo $OUTPUT->course_content_header();
					echo $OUTPUT->main_content();
				    echo $OUTPUT->course_content_footer();
				    ?>
				</div>

				<!-- Sidebar -->
				<div id="sidebar">
					<?php include('views/sidebar.php'); ?>
				</div>
	    	</div>
	    </div>

	    <!-- Content Top -->
		<?php include "views/content-bottom.php"; ?>
	</div>
	<!-- Id Page -->
	<!-- footer -->
	<?php include "partials/footer.php"; ?>
</body>
</html>
