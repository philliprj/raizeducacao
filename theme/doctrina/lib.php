<?php
defined('MOODLE_INTERNAL') || die();

// Libraries
require_once($CFG->dirroot . '/theme/doctrina/lib/category.php');
require_once($CFG->dirroot . '/theme/doctrina/lib/course.php');

// Pluginfile to get logo
function theme_doctrina_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = [])
{
    if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'logo') {
        $theme = theme_config::load('doctrina');
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }

        return $theme->setting_file_serve('logo', $args, $forcedownload, $options);
    } else {
        return false;
    }
}

function theme_doctrina_get_admin_categories_sql()
{
    global $DB;

    $query = <<<SQL
SELECT
  DISTINCT cat.id     categoryid,
  cat.name   name,
  cat.parent parent,
  cat.path   path
FROM mdl_course_categories cat
  JOIN mdl_course c ON c.category = cat.id
ORDER BY cat.sortorder
SQL;

    return $DB->get_records_sql($query);
}

function theme_doctrina_get_user_categories_sql()
{
    global $DB, $USER;

    $query = <<<SQL
SELECT
  DISTINCT cat.id     categoryid,
  cat.name   name,
  cat.parent parent,
  cat.path   path,
  enr.userid userid
FROM mdl_course_categories cat
  JOIN mdl_course c ON c.category = cat.id
  JOIN mdl_enrol e ON e.courseid = c.id
  JOIN mdl_user_enrolments enr ON enr.enrolid = e.id
WHERE cat.visible = :catvisible 
      AND c.visible = :cvisible 
      AND enr.status = :enrstatus
      AND enr.userid = :enruserid
      AND e.status = :estatus
ORDER BY cat.sortorder
SQL;

    $params = [
        'catvisible' => 1,
        'cvisible'   => 1,
        'enrstatus'  => 0,
        'enruserid'  => $USER->id,
        'estatus'    => 0,
    ];


    return $DB->get_records_sql($query, $params);
}

function theme_doctrina_get_user_categories($categoryselected)
{

    global $DB, $USER, $SESSION;

    $categories = [];

    $results = is_siteadmin($USER) ? theme_doctrina_get_admin_categories_sql() : theme_doctrina_get_user_categories_sql();

    if ($results) {

        $categoryids = [];
        foreach ($results as $key => $result) {
            $path = explode("/", $result->path);
            $categoryids[] = $path[1];
        }

        $categoryids = array_unique($categoryids);

        foreach ($categoryids as $categoryid) {
            $category = new \stdClass();
            $path = explode("/", $result->path);

            $category->id = $categoryid;
            $category->name = $DB->get_field('course_categories','name',['id' => $categoryid]);
            $category->active = false;

            if($categoryid == $categoryselected) {
                $category->active = true;
            }

            $categories[] = $category;
        }

        $SESSION->firstcategory = $categories[0]->id;
    }

    return $categories;
}

