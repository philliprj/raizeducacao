<?php

// Set the background image for the logo.
// ----------------------------
$header_logo = $PAGE->theme->setting_file_url('logo', 'logo');

// Footer Options
// ----------------------------
$columnCount = 12;
$footerCount = 0;

// Copyright
if(!empty($PAGE->theme->settings->ft_copyright)){
	$ft_copyright=$PAGE->theme->settings->ft_copyright ;
	$footerCount += 1;
}  else  {
    $ft_copyright  =  '' ;
}
// Contact
if(!empty($PAGE->theme->settings->ft_contact)){
	$ft_contact=$PAGE->theme->settings->ft_contact ;
	$footerCount += 1;
}  else  {
    $ft_contact  =  '' ;
}
// Social media links
if(!empty($PAGE->theme->settings->ft_social )){
	$ft_social =$PAGE->theme->settings->ft_social  ;
	$mediaLinks = explode(',', $ft_social );
	$footerCount += 1;
}  else  {
    $ft_social   =  '' ;
}

// Columns
if(!$footerCount == 0) {
	$countColumn = $columnCount / $footerCount;
} else {
	$countColumn = $columnCount;
}
