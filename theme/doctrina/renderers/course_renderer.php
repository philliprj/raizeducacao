<?php
/**
 * Renderers to align Moodle's HTML with that expected by doctrina
 *
 * @package    theme_doctrina
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . "/course/renderer.php");

class theme_doctrina_core_course_renderer extends core_course_renderer
{
    /**
     * Renders HTML to display a list of course modules in a course section
     * Also displays "move here" controls in Javascript-disabled mode
     *
     * This function calls {@link core_course_renderer::course_section_cm()}
     *
     * @param stdClass                  $course course object
     * @param int|stdClass|section_info $section relative section number or section object
     * @param int                       $sectionreturn section number to return to
     * @param int                       $displayoptions
     *
     * @return void
     */
    public function course_section_cm_list($course, $section, $sectionreturn = null, $displayoptions = [])
    {
        global $USER;

        $output = '';
        $modinfo = get_fast_modinfo($course);
        if (is_object($section)) {
            $section = $modinfo->get_section_info($section->section);
        } else {
            $section = $modinfo->get_section_info($section);
        }
        $completioninfo = new completion_info($course);

        // check if we are currently in the process of moving a module with JavaScript disabled
        $ismoving = $this->page->user_is_editing() && ismoving($course->id);
        if ($ismoving) {
            $movingpix = new pix_icon('movehere', get_string('movehere'), 'moodle', ['class' => 'movetarget']);
            $strmovefull = strip_tags(get_string("movefull", "", "'$USER->activitycopyname'"));
        }

        // Get the list of modules visible to user (excluding the module being moved if there is one)
        $moduleshtml = [];
        if (!empty($modinfo->sections[$section->section])) {
            foreach ($modinfo->sections[$section->section] as $modnumber) {
                $mod = $modinfo->cms[$modnumber];

                if ($ismoving and $mod->id == $USER->activitycopy) {
                    // do not display moving mod
                    continue;
                }

                if ($modulehtml = $this->course_section_cm_list_item($course,
                    $completioninfo, $mod, $sectionreturn, $displayoptions)
                ) {
                    $moduleshtml[$modnumber] = $modulehtml;
                }
            }
        }

        $sectionoutput = '';
        if (!empty($moduleshtml) || $ismoving) {
            foreach ($moduleshtml as $modnumber => $modulehtml) {
                if ($ismoving) {
                    $movingurl = new moodle_url('/course/mod.php', ['moveto' => $modnumber, 'sesskey' => sesskey()]);
                    $sectionoutput .= html_writer::tag('li',
                        html_writer::link($movingurl, $this->output->render($movingpix), ['title' => $strmovefull]),
                        ['class' => 'movehere']);
                }

                $sectionoutput .= $modulehtml;
            }

            if ($ismoving) {
                $movingurl = new moodle_url('/course/mod.php', ['movetosection' => $section->id, 'sesskey' => sesskey()]);
                $sectionoutput .= html_writer::tag('li',
                    html_writer::link($movingurl, $this->output->render($movingpix), ['title' => $strmovefull]),
                    ['class' => 'movehere']);
            }
        }

        // Always output the section module list.
        $output .= html_writer::tag('ul', $sectionoutput, ['class' => 'section img-text collapse in', 'id' => 'collapse-' . $section->id]);

        return $output;
    }

    /**
     * Página de categoria de curso
     *
     * @param coursecat|int|stdClass $category
     *
     * @return string
     * @throws Exception
     * @throws coding_exception
     * @throws dml_exception
     * @throws moodle_exception
     */
    public function course_category($category_id)
    {
        global $CFG, $PAGE, $OUTPUT;

        // Requer que o usuário esteja logado, com base nas configurações do site
        if ($CFG->forcelogin) {
            require_login();
        }

        // Listagem de todas as categorias considerando seus filhos (subcategorias)
        $categories = Category::get_categories($category_id);

        // Listagem de todos os cursos considerando seus filhos (subcategorias)
        $courses = Category::get_courses($category_id);

        // Retorna a categoria
        $category = Category::get($category_id);

        // Identifica as características do site
        $site = get_site();

        // Define características da página
        $PAGE->set_url('/course/index.php');
        $PAGE->set_context(null);

        // Define as características iniciais da página
        $PAGE->set_pagelayout('coursecategory');
        $PAGE->set_title($site->fullname);
        $PAGE->set_heading($site->fullname);

        // Setting breadcrumb
        Category::set_breadcrumb();
        echo $OUTPUT->header();
        include($CFG->dirroot . '/theme/doctrina/layout/coursecategory.php');
        echo $OUTPUT->footer();
        die;
    }

    protected function coursecat_coursebox_content(coursecat_helper $chelper, $course)
    {
        global $CFG;
        if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
            return '';
        }
        if ($course instanceof stdClass) {
            require_once($CFG->libdir . '/coursecatlib.php');
            $course = new course_in_list($course);
        }
        $content = '';

        // display course summary
        if ($course->has_summary()) {
            $content .= html_writer::start_tag('div', ['class' => 'summary']);
            $content .= $chelper->get_course_formatted_summary($course,
                ['overflowdiv' => true, 'noclean' => true, 'para' => false]);
            $content .= html_writer::end_tag('div'); // .summary
        }

        // display course overview files
        $contentimages = $contentfiles = '';
        foreach ($course->get_course_overviewfiles() as $file) {
            $isimage = $file->is_valid_image();
            $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                '/' . $file->get_contextid() . '/' . $file->get_component() . '/' .
                $file->get_filearea() . $file->get_filepath() . $file->get_filename(), !$isimage);
            if ($isimage) {
                $contentimages .= html_writer::tag('div',
                    html_writer::tag('span', '', ['class' => 'courseimage', 'style' => 'background-image: url(' . $url . ')']));
            } else {
                $image = $this->output->pix_icon(file_file_icon($file, 24), $file->get_filename(), 'moodle');
                $filename = html_writer::tag('span', $image, ['class' => 'fp-icon']) .
                    html_writer::tag('span', $file->get_filename(), ['class' => 'fp-filename']);
                $contentfiles .= html_writer::tag('span',
                    html_writer::link($url, $filename),
                    ['class' => 'coursefile fp-filename-icon']);
            }
        }
        $content .= $contentimages . $contentfiles;

        // display course contacts. See course_in_list::get_course_contacts()
        if ($course->has_course_contacts()) {
            $content .= html_writer::start_tag('ul', ['class' => 'teachers']);
            foreach ($course->get_course_contacts() as $userid => $coursecontact) {
                $name = $coursecontact['rolename'] . ': ' .
                    html_writer::link(new moodle_url('/user/view.php',
                        ['id' => $userid, 'course' => SITEID]),
                        $coursecontact['username']);
                $content .= html_writer::tag('li', $name);
            }
            $content .= html_writer::end_tag('ul'); // .teachers
        }

        // display course category if necessary (for example in search results)
        if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_EXPANDED_WITH_CAT) {
            require_once($CFG->libdir . '/coursecatlib.php');
            if ($cat = coursecat::get($course->category, IGNORE_MISSING)) {
                $content .= html_writer::start_tag('div', ['class' => 'coursecat']);
                $content .= get_string('category') . ': ' .
                    html_writer::link(new moodle_url('/course/index.php', ['categoryid' => $cat->id]),
                        $cat->get_formatted_name(), ['class' => $cat->visible ? '' : 'dimmed']);
                $content .= html_writer::end_tag('div'); // .coursecat
            }
        }

        return $content;
    }

    /**
     * Returns HTML to print list of available courses for the frontpage
     *
     * @return string
     */
    public function frontpage_available_courses()
    {
        global $CFG, $SESSION;
        require_once($CFG->libdir . '/coursecatlib.php');

        $categoryid = optional_param('category', $SESSION->firstcategory, PARAM_INT);

        $tabcategories = theme_doctrina_get_user_categories($categoryid);

        $chelper = new coursecat_helper();
        $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
        set_courses_display_options([
            'recursive'    => true,
            'limit'        => $CFG->frontpagecourselimit,
            'viewmoreurl'  => new moodle_url('/course/index.php'),
            'viewmoretext' => new lang_string('fulllistofcourses')]);

        $chelper->set_attributes(['class' => 'frontpage-course-list-all']);

        if(!isset($SESSION->firstcategory)){
            return $this->coursecat_courses($chelper, [], null, []);
        }


        $courses = coursecat::get($categoryid)->get_courses($chelper->get_courses_display_options());
        $totalcount = coursecat::get($categoryid)->get_courses_count($chelper->get_courses_display_options());
        if (!$totalcount && !$this->page->user_is_editing() && has_capability('moodle/course:create', context_system::instance())) {
            // Print link to create a new course, for the 1st available category.
            return $this->add_new_course_button();
        }

        return $this->coursecat_courses($chelper, $courses, $totalcount, $tabcategories);
    }

    /**
     * Renders the list of courses
     *
     * This is internal function, please use {@link core_course_renderer::courses_list()} or another public
     * method from outside of the class
     *
     * If list of courses is specified in $courses; the argument $chelper is only used
     * to retrieve display options and attributes, only methods get_show_courses(),
     * get_courses_display_option() and get_and_erase_attributes() are called.
     *
     * @param coursecat_helper $chelper various display options
     * @param array            $courses the list of courses to display
     * @param int|null         $totalcount total number of courses (affects display mode if it is AUTO or pagination if
     *     applicable), defaulted to count($courses)
     *
     * @return string
     */
    protected function coursecat_courses(coursecat_helper $chelper, $courses, $totalcount = null, $tabcategories = [])
    {
        global $CFG;
        if ($totalcount === null) {
            $totalcount = count($courses);
        }
        if (!$totalcount) {
            // Courses count is cached during courses retrieval.
            return '';
        }

        if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_AUTO) {
            // In 'auto' course display mode we analyse if number of courses is more or less than $CFG->courseswithsummarieslimit
            if ($totalcount <= $CFG->courseswithsummarieslimit) {
                $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED);
            } else {
                $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_COLLAPSED);
            }
        }

        // prepare content of paging bar if it is needed
        $paginationurl = $chelper->get_courses_display_option('paginationurl');
        $paginationallowall = $chelper->get_courses_display_option('paginationallowall');
        if ($totalcount > count($courses)) {
            // there are more results that can fit on one page
            if ($paginationurl) {
                // the option paginationurl was specified, display pagingbar
                $perpage = $chelper->get_courses_display_option('limit', $CFG->coursesperpage);
                $page = $chelper->get_courses_display_option('offset') / $perpage;
                $pagingbar = $this->paging_bar($totalcount, $page, $perpage,
                    $paginationurl->out(false, ['perpage' => $perpage]));
                if ($paginationallowall) {
                    $pagingbar .= html_writer::tag('div', html_writer::link($paginationurl->out(false, ['perpage' => 'all']),
                        get_string('showall', '', $totalcount)), ['class' => 'paging paging-showall']);
                }
            } else if ($viewmoreurl = $chelper->get_courses_display_option('viewmoreurl')) {
                // the option for 'View more' link was specified, display more link
                $viewmoretext = $chelper->get_courses_display_option('viewmoretext', new lang_string('viewmore'));
                $morelink = html_writer::tag('div', html_writer::link($viewmoreurl, $viewmoretext),
                    ['class' => 'paging paging-morelink']);
            }
        } else if (($totalcount > $CFG->coursesperpage) && $paginationurl && $paginationallowall) {
            // there are more than one page of results and we are in 'view all' mode, suggest to go back to paginated view mode
            $pagingbar = html_writer::tag('div', html_writer::link($paginationurl->out(false, ['perpage' => $CFG->coursesperpage]),
                get_string('showperpage', '', $CFG->coursesperpage)), ['class' => 'paging paging-showperpage']);
        }

        // display list of courses
        $attributes = $chelper->get_and_erase_attributes('courses');

        $content = $this->render_from_template('theme_doctrina/menu_categories', ["categories" => $tabcategories]);

        $content .= html_writer::start_tag('div', $attributes);

        if (!empty($pagingbar)) {
            $content .= $pagingbar;
        }

        $coursecount = 0;
        foreach ($courses as $course) {
            $coursecount++;
            $classes = ($coursecount % 2) ? 'odd' : 'even';
            if ($coursecount == 1) {
                $classes .= ' first';
            }
            if ($coursecount >= count($courses)) {
                $classes .= ' last';
            }
            $content .= $this->coursecat_coursebox($chelper, $course, $classes);
        }

        if (!empty($pagingbar)) {
            $content .= $pagingbar;
        }
        if (!empty($morelink)) {
            $content .= $morelink;
        }

        $content .= html_writer::end_tag('div'); // .courses
        return $content;
    }
}
