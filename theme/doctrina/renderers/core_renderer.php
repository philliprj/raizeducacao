<?php
defined('MOODLE_INTERNAL') || die();

/**
 * Renderers to align Moodle's HTML with that expected by doctrina
 *
 * @package    theme_doctrina
 * @copyright  2016
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class theme_doctrina_core_renderer extends core_renderer {
	protected function render_custom_menu_item(custom_menu_item $menunode, $level = 0, $direction = '' ) {
	    static $submenucount = 0;

	    if ($menunode->has_children()) {

	        if ($level == 1) {
	            $dropdowntype = 'dropdown';
	        } else {
	            $dropdowntype = 'dropdown-submenu';
	        }

	        $content = html_writer::start_tag('li', array('class' => $dropdowntype));
	        // If the child has menus render it as a sub menu.
	        $submenucount++;
	        if ($menunode->get_url() !== null) {
	            $url = $menunode->get_url();
	        } else {
	            $url = '#cm_submenu_'.$submenucount;
	        }
	        $linkattributes = array(
	            'href' => $url,
	            'class' => 'dropdown-toggle',
	            'data-toggle' => 'dropdown',
	            'title' => $menunode->get_title(),
	        );
	        $content .= html_writer::start_tag('a', $linkattributes);
	        $content .= $menunode->get_text();
	        if ($level == 1) {
	            $content .= '<b class="caret"></b>';
	        }
	        $content .= '</a>';
	        $content .= '<ul class="dropdown-menu '.$direction.'">';
	        foreach ($menunode->get_children() as $menunode) {
	            $content .= $this->render_custom_menu_item($menunode, 0);
	        }
	        $content .= '</ul>';
	    } else {
	        $content = '<li>';
	        // The node doesn't have children so produce a final menuitem.
	        $class = $menunode->get_title();
	        if (preg_match("/^#+$/", $menunode->get_text())) {
	            $content = '<li class="divider" role="presentation">';
	        } else {
	            $content = '<li>';
	            // The node doesn't have children so produce a final menuitem.
	            if ($menunode->get_url() !== null) {
	                $url = $menunode->get_url();
	            } else {
	                $url = '#';
	            }
	            $content .= html_writer::link($url, $menunode->get_text(), array('class' => $class,
	                'title' => $menunode->get_title()));
	        }
	    }
	    return $content;
	}

	private function glyphicon($icon) {
	    $icon = html_writer::tag('i', '', array('class' => 'glyphicon glyphicon-' . $icon));
	    return html_writer::tag('span', $icon, array('class' => 'iconwrapper'));
	}
}
