// Sidebar Nav
doctrina.sidebar_nav = function() {
	// Button
	var bt = $('.toggle-sidebar-menu');

	// Add class in 'body' tag to open sidebar via class
	bt.on('click', function(e){
		e.preventDefault();
		$('body').toggleClass('sidebar-opened');
	});

	$(document).on('click', '*', function(event) {
	    event.stopPropagation();
	    if (!$(this).is('#sidebar, #sidebar *')) {
	    	$('body').removeClass('sidebar-opened');
	    }
	});

	$('#header').on('click', function(){
	  $('body').removeClass('sidebar-opened');
	})
}
