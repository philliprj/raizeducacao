$(document).ready(function() {
	// Dot Dot
	$('.dotdot, .coursebox .content .summary, .coursebox .coursename a').dotdotdot({
		watch: 'document'
	});

	// Sidebar
	doctrina.sidebar_nav();

	// Frontpage
	doctrina.frontpage.slideshow({dots: false, arrows: true});
	doctrina.frontpage.courselist.getBtn();
	doctrina.frontpage.selectCourse();
});
