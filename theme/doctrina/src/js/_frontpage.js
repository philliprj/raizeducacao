doctrina.frontpage = {
	slideshow: function(options) {
		$('#slideshow .slides').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: options.arrows,
			dots: options.dots
		});
	},
	courselist: {
		getBtn: function() {
			if ($(window).width() <= 991) {
				$('.frontpage-course-list-all .coursebox').each(function() {
					var url = $(this).find('.coursename a').attr('href');
					$(this).append('<a class="btn-more btn btn-primary" href=' + url + '>Ver Curso</a>');
				});
			}
		}
	},
	selectCourse: function() {
		var url_string = window.location.href;
		var url = new URL(url_string);
		var id = url.searchParams.get("category");

		$('#select-course').val(id);

		$('#select-course').on('change', function() {
			var id = $(this).val();
			location.href = '?category=' + id;
		});
	}
};
